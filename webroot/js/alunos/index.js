function removerAluno(idAluno) {

    Swal.fire({
        title: 'Você tem certeza?',
        text: "O aluno com id " + idAluno + " será removido!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Remover',
        cancelButtonText: 'Cancelar'
    }).then((result) => {

        if (result.isConfirmed) {

            $.ajax({
                type: 'POST',
                url: "alunos/delete",
                data: {'idAluno'          : idAluno,
                       '_Token[fields]'   : $('[name="_Token[fields]"]').val(),
                       '_Token[unlocked]' : $('[name="_Token[unlocked]"]').val(),
                       '_Token[debug]'    : $('[name="_Token[debug]"]').val()},
                dataType: 'json',
                beforeSend: function(xhr){
                    xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                },
                success: function(response) {

                    Swal.fire({
                        icon: 'success',
                        title: response.message,
                        confirmButtonText: 'Ok',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    }).then((result) => {

                        if(result.isConfirmed) {
                            $("#trAluno" + idAluno).fadeOut(1500, function() {
                                $(this).remove();
                            });

                            setTimeout(function() { 
                                $(".container").load(window.location.href); 
                            }, 1000);
                        }
                    });
                },
                error: function(response) {

                    Swal.fire({
                        icon: 'error',
                        title: response.responseJSON.message,
                        confirmButtonText: 'Ok',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    }).then((result) => {

                        if(result.isConfirmed)
                            window.location.reload();
                    });
                }
            });
        }
    });
}