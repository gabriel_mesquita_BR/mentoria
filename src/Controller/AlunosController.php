<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Alunos Controller
 *
 * @property \App\Model\Table\AlunosTable $Alunos
 * @method \App\Model\Entity\Aluno[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AlunosController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $alunos = $this->paginate($this->Alunos);

        $this->set(compact('alunos')); // ['alunos' => $alunos]
    }

    /**
     * View method
     *
     * @param string|null $id Aluno id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $aluno = $this->Alunos->get($id);

        $this->set(compact('aluno'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $aluno = $this->Alunos->newEmptyEntity();

        if ($this->request->is('post')) {

            $aluno = $this->Alunos->patchEntity($aluno, $this->request->getData());

            if ($this->Alunos->save($aluno)) {

                $this->Flash->success(__('Aluno salvo com sucesso!'));

                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(__('Aluno não foi salvo, por favor tente novamente!'));
        }

        $this->set(compact('aluno'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Aluno id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $aluno = $this->Alunos->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {

            $aluno = $this->Alunos->patchEntity($aluno, $this->request->getData());

            if ($this->Alunos->save($aluno)) {

                $this->Flash->success(__('Aluno atualizado com sucesso!'));

                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(__('Aluno não foi atualizado, por favor tente novamente!'));
        }

        $this->set(compact('aluno'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Aluno id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        // $this->request->allowMethod(['post', 'delete']);

        // $aluno = $this->Alunos->get($id);

        // if ($this->Alunos->delete($aluno)) {
        //     $this->Flash->success(__('Aluno removido com sucesso!'));
        // } else {
        //     $this->Flash->error(__('Aluno não foi removido, por favor tente novamente!'));
        // }

        // return $this->redirect(['action' => 'index']);

        try {

            $id = $this->request->getData('idAluno');

            $this->request->allowMethod(['post', 'delete']);

            $aluno = $this->Alunos->get($id);

            $this->Alunos->delete($aluno);
                
            $this->set(['message' => 'Aluno removido com sucesso!']);
            
            $this->viewBuilder()->setOption('serialize', true);
            $this->RequestHandler->renderAs($this, 'json');

        }catch(\Exception $exception) {
            throw new \Exception('Aluno não foi removido, por favor tente novamente!');
        }
    }
}
