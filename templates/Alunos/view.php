<div class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-5">
                <h3>
                    <?= __('Visualizar Aluno'); ?>
                    
                    <?= $this->Html->link('<i class="fa fa-bars"></i>', 
                        ['action' => 'index'], ['class'=>'btn btn-info btn-flat btn-sm', 
                         'escape' => false, 'title'=>'Listar Alunos']); ?>
                    
                    <?= $this->Html->link('<i class="fa fa-edit"></i>', 
                        ['action' => 'edit', $aluno->id], 
                        ['class'=>'btn btn-warning btn-flat btn-sm', 'escape' => false,
                         'title'=>'Editar Aluno']); ?>
                </h3>
            </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="mt-3 text-center">
                <h3 class="card-title">
                    Dados Detalhados - ID: <?= $aluno->id ?>
                </h3>
            </div>
            
            <div class="card-body">
                <div class="row">
                    <label class="col-sm-3 col-md-3 col-lg-3 col-xl-3 control-label">
                        <?= __('Nome'); ?>
                    </label>

                    <div class="col-sm-9 col-md-9 col-lg-9 col-xl-9">
                        <?= h($aluno->nome) ?>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-3 col-md-3 col-lg-3 col-xl-3 control-label">
                        <?= __('Data de Nascimento'); ?>
                    </label>

                    <div class="col-sm-9 col-md-9 col-lg-9 col-xl-9">
                        <?= h($aluno->data_nascimento) ?>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-3 col-md-3 col-lg-3 col-xl-3 control-label">
                        <?= __('Gênero'); ?>
                    </label>

                    <div class="col-sm-9 col-md-9 col-lg-9 col-xl-9">
                        <?= $aluno->genero === 'M' ? h('Masculino') : h('Feminino') ?>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-3 col-md-3 col-lg-3 col-xl-3 control-label">
                        <?= __('Matrícula'); ?>
                    </label>

                    <div class="col-sm-9 col-md-9 col-lg-9 col-xl-9">
                        <?= h($aluno->matricula) ?>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-3 col-md-3 col-lg-3 col-xl-3 control-label">
                        <?= __('Email'); ?>
                    </label>

                    <div class="col-sm-9 col-md-9 col-lg-9 col-xl-9">
                        <?= h($aluno->email) ?>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-3 col-md-3 col-lg-3 col-xl-3 control-label">
                        <?= __('Criação do Registro'); ?>
                    </label>

                    <div class="col-sm-9 col-md-9 col-lg-9 col-xl-9">
                        <?= h($aluno->created) ?>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-3 col-md-3 col-lg-3 col-xl-3 control-label">
                        <?= __('Atualização do Registro'); ?>
                    </label>

                    <div class="col-sm-9 col-md-9 col-lg-9 col-xl-9">
                        <?= h($aluno->modified) ?>
                    </div>
                </div>   
            </div>       
        </div>
    </div>
</section>
