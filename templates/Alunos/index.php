<?php
    echo $this->Html->css('alunos/index');
    echo $this->Html->script('alunos/index');
?>

<div class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-5">
                <h3>
                    <?= __('Alunos') ?>
                    
                    <?= $this->Html->link('<i class="fa fa-plus-circle"></i>', 
                        ['action' => 'add'], ['class'=>'btn btn-info btn-flat btn-sm', 
                         'escape' => false, 'title'=>'Adicionar Novo Aluno']); ?>
                </h3>
            </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="mt-3 text-center">
                <h3 class="card-title">Listagem Alunos</h3>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 5%;">
                                    <?= $this->Paginator->sort('id', 'Id') ?>
                                </th>
                                <th class="text-center" style="width: 10%;">
                                    <?= $this->Paginator->sort('nome', 'Descrição') ?>
                                </th>
                                <th class="text-center" style="width: 40%;">
                                    <?= $this->Paginator->sort('data_nascimento', 
                                        'Data de Nascimento') ?>
                                </th>
                                <th class="text-center" style="width: 40%;">
                                    <?= $this->Paginator->sort('genero', 
                                        'Gênero') ?>
                                </th>
                                <th class="text-center" style="width: 40%;">
                                    <?= $this->Paginator->sort('matricula', 
                                        'Matrícula') ?>
                                </th>
                                <th class="text-center" style="width: 40%;">
                                    <?= $this->Paginator->sort('email', 
                                        'Email') ?>
                                </th>
                                <th class="text-center">
                                    <?= $this->Paginator->sort('created', 'Criado') ?>
                                </th>
                                <th class="text-center">
                                    <?= $this->Paginator->sort('modified', 'Modificado') ?>
                                </th>
                                <th style="color: #007bff; width: 15%;" class="text-center">
                                    <?= __('Opções') ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($alunos as $aluno): ?>
                                <tr id="trAluno<?= $aluno->id ?>">
                                    <td><?= $this->Number->format($aluno->id) ?></td>
                                    <td>
                                        <?= h($aluno->nome) ?>
                                    </td>
                                    <td>
                                        <?= h($aluno->data_nascimento) ?>
                                    </td>
                                    <td>
                                        <?= $aluno->genero === 'M' ? h('Masculino') :
                                            h('Feminino')
                                        ?>
                                    </td>
                                    <td>
                                        <?= h($aluno->matricula) ?>
                                    </td>
                                    <td>
                                        <?= h($aluno->email) ?>
                                    </td>
                                    <td><?= h($aluno->created) ?></td>
                                    <td><?= h($aluno->modified) ?></td>
                                    <td class="text-center">
                                        <div class="btn-group">

                                            <?php echo $this->Html->link('
                                                <i class="fa fa-info-circle"></i>', 
                                                ['action' => 'view', $aluno->id], 
                                                ['class'=>'btn btn-info btn-flat btn-sm', 
                                                 'escape' => false, 
                                                 'title'=>'Visualizar Detalhes']); 
                                            ?>

                                            <?php echo $this->Html->link('
                                                <i class="fa fa-edit"></i>', 
                                                ['action' => 'edit', $aluno->id],
                                                ['class'=>'btn btn-warning btn-flat btn-sm', 
                                                 'escape' => false, 'title'=>'Editar']); 
                                            ?>
                                                
                                            <!-- <php echo $this->Form->postLink(
                                                '<i class="fa fa-minus-circle"></i>',
                                                ['action' => 'delete', $aluno->id],
                                                ['class'=>'btn btn-danger btn-flat btn-sm',
                                                 'escape' => false, 'title'=>'Remover',
                                                 'confirm' => __('Você tem certeza que deseja remover o aluno de id {0}?', 
                                                 $aluno->id)]);
                                            ?> -->

                                            <?= $this->Form->create(null, ['url' => 
                                                    ['action' => 'delete']]); 
                                                    
                                                $this->Form->unlockField('idAluno')
                                            ?>

                                            <a class="btn btn-danger btn-flat btn-sm"
                                                title='Apagar'
                                                style="cursor: pointer; color: #fff;"
                                                onclick="removerAluno(
                                                    <?= json_encode($aluno->id) ?>)">
                                                <i class="fa fa-minus-circle"></i>
                                            </a>

                                            <?= $this->Form->end(); ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="dataTables_paginate paging_simple_numbers"
                        style="margin: 0 auto;">
                        <ul class="pagination">
                            <?= $this->Paginator->prev('
                                <i class="fa fa-chevron-circle-left">
                                    </i>&nbsp;&nbsp;' . __('Antes'), 
                                    ['escape' => false]) ?>
                            <?= $this->Paginator->numbers() ?>
                            <?= $this->Paginator->next(__('Depois') . '&nbsp;&nbsp;
                                <i class="fa fa-chevron-circle-right"></i>', 
                                ['escape' => false]) ?>
                        </ul>
                    </div>
                </div>

                <div class="row">
                    <p style="margin: 0 auto;">
                        <?= $this->Paginator->counter(__('Página <b>{{page}}</b> de 
                            <b>{{pages}}</b>, mostrando <b>{{current}}</b> registros de 
                            <b>{{count}}</b> (total) - Registros: <b>{{start}}&ordm;</b> 
                            a <b>{{end}}&ordm;</b>')) ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
