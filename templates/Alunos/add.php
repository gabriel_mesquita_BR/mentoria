<div class="content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-5">
                <h3>
                    <?= __('Adicionar Aluno'); ?>
                    
                    <?= $this->Html->link('<i class="fa fa-bars"></i>', 
                        ['action' => 'index'], ['class'=>'btn btn-info btn-flat btn-sm', 
                        'escape' => false, 'rel'=>'tooltip', 'data-placement'=>'top', 
                        'title'=>'Listar Alunos']); ?>
                </h3>
            </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-title">
                <h5 class="text-center">Preencha os dados solicitados abaixo</h5>
            </div>

            <div class="card-body">
                <?php 
                    echo $this->Form->create($aluno);
                    
                    echo $this->Form->control('nome', ['class' => 'form-control mb-3',
                        'placeholder' => 'Digite o nome']);

                    echo $this->Form->control('data_nascimento', 
                        ['label' => 'Data de Nascimento', 'class' => 'form-control mb-3',
                         'placeholder' => 'Digite a data de nascimento']);

                    echo $this->Form->control('genero', 
                        ['label' => 'Gênero', 'class' => 'form-control mb-3', 
                         'type' => 'select', 
                         'options' => ['M' => 'Masculino', 'F' => 'Feminino'],
                         'empty' => false]);

                    echo $this->Form->control('matricula', 
                        ['label' => 'Matrícula', 'class' => 'form-control mb-3', 
                         'placeholder' => 'Digite a matrícula']);

                    echo $this->Form->control('email', ['class' => 'form-control mb-3',
                        'placeholder' => 'Digite o email']);
                ?>
            </div>
            
            <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
                    <?= $this->Html->link('Cancelar', ['action' => 'index'], 
                        ['class'=>'btn btn-danger']); ?>
                </div>

                <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
                    <?= $this->Form->Submit(__('Adicionar'), ['div'=>false, 
                        'class'=>'btn btn-info float-right']); ?>
                </div>
            </div>
            
            <?= $this->Form->end(); ?>
        </div>
    </div>
</section>
